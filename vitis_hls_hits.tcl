#---------------------------------------------------------------
# Compile HLS IP core for reading stubs file .txt file
# & streaming them out.
#---------------------------------------------------------------

delete_project WorkHLS_hits
open_project WorkHLS_hits

set CFLAGS {-std=c++11}

set_top readHits
add_files     src_hits/readHits.h     -cflags "$CFLAGS"
add_files     src_hits/readHits.cc    -cflags "$CFLAGS"
add_files -tb src_hits/tb.cc          -cflags "$CFLAGS"

set solution solution1
delete_solution $solution
open_solution -flow_target vivado $solution

# Nexys-A7 100
set_part {xc7a100tcsg324-1}

create_clock -period 100MHz -name default

# Allow HLS to use longer (easier to understand) variable names in reports.
config_compile -name_max_length 100
# Register output signals, to ensure they're stable (at cost of +1 latency).
config_interface -register_io scalar_out 

csim_design -clean -mflags "-j8"
csynth_design
cosim_design -trace_level all
export_design -format ip_catalog -version 0.0.0

exit
