#include <iostream>

#include "readHits.h"

int main() {

  ap_uint<1> last = 0;
  ap_uint<NBITS_HIT> hit = nullHit;

  //readHits(false, output);
  //%printf("Result before enable=%lld\n",(long long)output.read());

  unsigned int iErr = 0;

  for (unsigned int idx=0; idx<=numHits; idx++) {

    // Get expected output of of HLS module.
    ap_uint<1> last_expected = (idx >= numHits - 1); 
    ap_uint<NBITS_HIT> hit_expected = (idx < numHits) ? hits[idx] : nullHit;

    readHits(hit, last);

    std::cout<<std::dec<<" Index = "<<idx<<" last = "<<last<<" hit = "<<std::hex<<hit<<" vs expected "<<hit_expected;
    std::cout<<std::dec<<" Layer = "<<hit.range(MSB_LAY, LSB_LAY)<<std::endl;
 
    if (hit != hit_expected || last != last_expected) iErr++;
  }
  return iErr;
}

