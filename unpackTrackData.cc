#include <iostream>
#include <fstream>
#include <string>
#include <cmath>

//--- Unpack data file written over UART serial link from FPGA to Linux 
//--- computer, to extract the helix parameters of each reconstruted track.

int main() {

  const char* fileName = "myFile.txt";
  const unsigned int nBits = 16; // No. of bits per helix parameter.
  const unsigned int mask = (1 << nBits) - 1;

  //--- Copy from hitFormat.h

  // Digitization granularity in r, phi & z.
  const float hit_granularity = 0.001;

  //--- Copy from src_trk/trkReco.h

  const float bField = 4.0; // Magnetic field (Tesla)
  const float cLight = 3.0e8; // Speed of light (m/s)
  // Number of bins in Hough Transform (HT).
  const int nBinsR = 16, nBinsPhi0 = 128;
  // Bit shifts to correct for relative granularity of stub coords.
  // & helix params.
  // Adjusting (0,13) here will change the HT range in (1/R,phi0)
  const int shiftInvR =  0+std::round(std::log2(1.*nBinsR));
  const int shiftPhi0 = 13+std::round(std::log2(1./nBinsPhi0));


  std::ifstream inFile( fileName , std::ifstream::in );
  //std::string data;
  unsigned int data;
  if (inFile) {
    while (inFile >> std::hex >> data) {
      short int phi0_digi = data & mask;
      short int invR_digi = 2 * (data >> nBits) & mask;
      float phi0 = float(phi0_digi) * (hit_granularity*(1 << shiftPhi0));
      std::cout<<invR_digi<<" "<<shiftInvR<<std::endl;
      float invR = float(invR_digi) / (1 << shiftInvR);
      float qOverPt = invR / (bField * cLight / 1.0e9);
      std::cout<<"digi_phi0 = "<<phi0_digi<<", digi_invR = "<<invR_digi<<"; phi0 (rads) = "<<phi0<<", invR (1/m) = "<<invR<<", q/Pt (1/GeV) = "<<qOverPt<<std::endl;
    }
  } else {
    std::cout<<"ERROR: file "<<fileName<<" does not exist"<<std::endl;
  }
}
  
