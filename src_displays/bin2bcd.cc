#include "ap_int.h"
#include "bin2bcd.h"

// Function to convert binary data to BCD format.
void bin2bcd(in_data_t value, bcd_t* out_data, int cant_display)
{
	for( int i=0; i < cant_display;i++)
	{
		out_data[i] = (bcd_t) (value % 10 );
		value = (in_data_t) (value / 10);
	}
	return;
}

