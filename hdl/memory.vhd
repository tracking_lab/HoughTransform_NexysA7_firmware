library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--------------------------------------------------------------
-- A dual-clock BRAM memory
-- with fast clock for write & slow clock for read.
--------------------------------------------------------------

-- Provides arithmetic for signed & unsigned values
use IEEE.NUMERIC_STD.ALL;

entity memory is
    Generic ( WIDTH_BITS : natural;
    	      DEPTH_BITS : natural);
    Port ( READ_CLK     : in  std_logic;
	   READ_ENABLE  : in  std_logic;
	   READ_ADDR    : in  std_logic_vector(DEPTH_BITS - 1 downto 0);
	   READ_DATA    : out std_logic_vector(WIDTH_BITS - 1 downto 0);
	   WRITE_CLK    : in  std_logic;
	   WRITE_ENABLE : in  std_logic;
	   WRITE_ADDR   : in  std_logic_vector(DEPTH_BITS - 1 downto 0);
	   WRITE_DATA   : in  std_logic_vector(WIDTH_BITS - 1 downto 0));
end memory;

architecture Behavioral of memory is

  constant DEPTH : natural := 2**DEPTH_BITS;
  type memType is array(0 to DEPTH-1) of std_logic_vector(WIDTH_BITS-1 downto 0);
  signal RAM : memType := (others => (others => '0'));
  attribute ram_style : string;
  attribute ram_style of RAM : signal is "block";
  
begin

memAccess : process (READ_CLK, WRITE_CLK)
begin
  if rising_edge(READ_CLK) then
    if READ_ENABLE = '1' then
       READ_DATA <= RAM(to_integer(unsigned(READ_ADDR)));
    end if;
  end if;
  if rising_edge(WRITE_CLK) then
    if WRITE_ENABLE = '1' then
       RAM(to_integer(unsigned(WRITE_ADDR))) <= WRITE_DATA;
    end if;
  end if;
end process;  

end Behavioral;
