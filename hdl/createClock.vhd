library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-----------------------------------------------------------------
-- Convert 100 MHz clock generated on board &
-- arriving at input pins of FPGA into single-line 100 MHz clock
-- and single-line 6.25 MHz clock for UART
-----------------------------------------------------------------

-- Provides arithmetic for signed & unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Provides utilities IBUFDS & MMCME2_BASE
library UNISIM;
use UNISIM.VComponents.all;

entity createClock is
    Port ( SYSCLK_IN  : in  std_logic;   --! 100 MHz clock
           CLK        : out std_logic;   --! 100 MHz clock
           CLKslow    : out std_logic);  --! 100/16 = 6.25 MHz clock           
end createClock;

architecture Behavioral of createClock is
  signal SYSCLK :   std_logic := '0';
  signal PWRDWN :   std_logic := '0';
  signal RST :      std_logic := '0';
  signal CLKFBIN :  std_logic := '0';
  signal CLKFBOUT : std_logic := '0';

begin

  -- (ibuf is function defined in UNISIM library).

  clock_inst : ibuf
    generic map (
      --! Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards.
      IBUF_LOW_PWR => FALSE,
      --! Assigns an I/O standard to the element.
      IOSTANDARD   => "DEFAULT"
    )
    port map(
      I  => SYSCLK_IN,
      O  => SYSCLK
    );

-- MMCME2_BASE: Base Mixed Mode Clock Manager (MMCM)
-- Xilinx Artix-7 (7 series)
-- https://www.xilinx.com/support/documentation/user_guides/ug472_7Series_Clocking.pdf

CLKFBIN <= CLKFBOUT;

MMCME : MMCME2_BASE
-- Frequency of output clock 1 scaled by CLKFBOUT_MULT_F/CLKOUT1_DIVIDE
-- relative to 100 MHz input clock.
-- So this gives 100 MHz output clock 1.
-- Similarly, gives also 6.25 MHz output clock 2.
-- And 1 MHz output clock 3.
generic map (
  CLKFBOUT_MULT_F => 8.0,  -- Multiply value for all CLKOUT (3.0-64.0)
  CLKIN1_PERIOD   => 10.0, -- Input clock period in ns units
  CLKOUT1_DIVIDE  => 8,    -- Divide amount for each CLKOUT1 (1-128)
  CLKOUT2_DIVIDE  => 128   -- Divide amount for each CLKOUT2 (1-128)  
)

port map (
  PWRDWN   => PWRDWN,      -- 1-bit input:  Power-down
  RST      => RST,         -- 1-bit input:  Reset
  CLKIN1   => SYSCLK,      -- 1-bit input:  Clock
  CLKOUT1  => CLK,         -- 1-bit output: CLKOUT1
  CLKOUT2  => CLKslow,     -- 1-bit output: CLKOUT2
  CLKFBIN  => CLKFBIN,     -- 1-bit input:  Feedback clock
  CLKFBOUT => CLKFBOUT     -- 1-bit output: Feedback clock
);

end Behavioral;
