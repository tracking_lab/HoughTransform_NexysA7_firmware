library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Provides arithmetic for signed & unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--================================================================
-- Compile top-level VHDL that instantiates all the HLS IP cores
-- Runs track reconstruction, stores found tracks in a BRAM
-- and transmits contents of BRAM over USB/UART to Linux.
--================================================================

entity top is
    Port ( CLK100MHZ  : in  std_logic;     --! 100 MHz clock for Nexys A7 - 100T
           
	   BTNC  : in  std_logic;              --! Push button
	   UART_RXD_OUT   : out std_logic;     --! USB-RS232 UART transmit signal (confusingly called RX)
           LED_0 : out std_logic; --! LED lights
           LED_1 : out std_logic;
           LED_2 : out std_logic;
           LED_3 : out std_logic;
           LED_4 : out std_logic;

           --! 7-SEGMENT DISPLAY
    	   CA : out std_logic;
           CB : out std_logic;
           CC : out std_logic;
           CD : out std_logic;
           CE : out std_logic;
           CF : out std_logic;
           CG : out std_logic;
           DP : out std_logic;
	   AN : out std_logic_vector(7 downto 0)); --! Display selection
end top;

architecture Behavioral of top is

-- Start program by pressing button on board (or automatically when program loaded into FPGA)?
constant USE_BUTTON : boolean := FALSE;

-- Size of RAM for storing tracks
constant WIDTH_BITS : natural := 32;
constant DEPTH_BITS : natural := 8;
-- Width in bits of a hit;
constant HIT_BITS : natural := 64;

-- FPGA clock (single line) and slow clock (scaled down by factor 16)
signal CLK, CLKslow : std_logic := '0';

-- Button press on board launches track reco (if USE_BUTTON = TRUE).
signal BTN, START_BTN : std_logic := '0';

-- Control signals for HLS IP Cores.
signal RESET, RESET_REG1, RESET_REG2 : std_logic := '1';
signal START_HIT, RESET_HIT, READY_HIT, LATCH_READY_HIT : std_logic := '0';
signal START_TRK, READY_TRK, DONE_TRK, FINISHED_TRK, FINISHED_TRK_LATCH, FINISHED_TRK_REG1, FINISHED_TRK_REG2, DONE_TRK_READ_HITS : std_logic := '0';
signal FINISHED_COUNT : std_logic := '0';
signal READY_UART,  DONE_UART, FINISHED_UART : std_logic := '0';

-- Signals of hit FIFO.
signal READ_FIFO, LAST_HIT : std_logic := '0';
signal HIT : std_logic_vector(HIT_BITS - 1 downto 0) := (others => '0'); 

-- Signal sent along UART/USB link to Linux.
signal UART_TX : std_logic := '1';
signal UART_TX_LATCH : std_logic := '0'; -- Says if there has been any UART/USB activity.

-- Data access to BRAM memory used to store reconstructed tracks.
signal READ_ENABLE  : std_logic := '0';
signal READ_ADDR    : std_logic_vector(DEPTH_BITS - 1 downto 0) := (others => '0');
signal READ_DATA    : std_logic_vector(WIDTH_BITS - 1 downto 0) := (others => '0');
signal WRITE_ENABLE : std_logic := '0';
signal WRITE_ADDR   : std_logic_vector(DEPTH_BITS - 1 downto 0) := (others => '0');
signal WRITE_DATA   : std_logic_vector(WIDTH_BITS - 1 downto 0) := (others => '0');

signal NTRACKS, NTRACKS_REG1, NTRACKS_REG2 : std_logic_vector(DEPTH_BITS - 1 downto 0) := (others => '0');

-- 7-segment display signals
signal DISPLAY_IN_VALUE  : std_logic_vector(26 downto 0) := (others => '0');
signal DISPLAY_SEL       : std_logic_vector(7  downto 0) := (others => '0');
signal DISPLAY_OUT       : std_logic_vector(7  downto 0) := (others => '0');

begin

-- Map UART transmission signal to more sensible name.
UART_RXD_OUT <= UART_TX;

-- Map button to simpler name.
BTN <= BTNC;

-- Connect LED lights to chosen signals.
LED_0 <= START_HIT;
LED_1 <= FINISHED_TRK;
LED_2 <= FINISHED_UART;
LED_3 <= UART_TX;
LED_4 <= UART_TX_LATCH;

-- Connect signals to display IP
DISPLAY_IN_VALUE(DEPTH_BITS-1 downto 0 ) <= NTRACKS_REG2;
DISPLAY_IN_VALUE(26 downto DEPTH_BITS)    <= (others => '0');

AN <= NOT(DISPLAY_SEL);

CA <= NOT(DISPLAY_OUT(0)); 
CB <= NOT(DISPLAY_OUT(1));
CC <= NOT(DISPLAY_OUT(2));
CD <= NOT(DISPLAY_OUT(3));
CE <= NOT(DISPLAY_OUT(4));
CF <= NOT(DISPLAY_OUT(5));
CG <= NOT(DISPLAY_OUT(6));
DP <= NOT(DISPLAY_OUT(7));

-- Create clock signals from clock signal arriving at FPGA.

makeClock : entity work.createClock
  port map (
     SYSCLK_IN  => CLK100MHZ,
     CLK        => CLK,
     CLKslow    => CLKslow
  );


-- Latch some signals,

START_TRK <= READY_HIT or LATCH_READY_HIT;

latches : process (CLK, CLKslow)
begin

  if rising_edge(CLK) then
    -- Hit FIFO read latency is 1, so start track reco 1 clk after 
    -- FIFO starts outputting hits.
    if (READY_HIT = '1') then
      LATCH_READY_HIT <= READY_HIT;
    end if;

    -- Note if track reco is finished.
    -- DONE says step finished, but it is undefined until READY signal asserted.
    if (READY_TRK = '1' and DONE_TRK = '1') then
      FINISHED_TRK  <= '1';
    end if;
  end if;

  if rising_edge(CLKslow) then
    -- Notes if UART tranmission is finished.
    if (READY_UART = '1' and DONE_UART = '1') then
      FINISHED_UART <= '1';
    end if;

    -- Records if there was ever any activity on the USB/UART line.
    if UART_TX = '0' then
      UART_TX_LATCH <= '1';
    end if; 
  end if;

end process;


-- Pass signals across boundary from CLK to CLKslow domain.

crossing : process (CLK, CLKslow)
begin

  if rising_edge(CLK) then
    if FINISHED_TRK = '1' then
      FINISHED_TRK_LATCH <= '1';
    end if;     
  end if;

  if rising_edge(CLKslow) then
    -- The trick is to register each of them twice in succession.
    RESET_REG1 <= RESET;
    RESET_REG2 <= RESET_REG1;
    FINISHED_TRK_REG1 <= FINISHED_TRK_LATCH;    
    FINISHED_TRK_REG2 <= FINISHED_TRK_REG1;    
    NTRACKS_REG1 <= NTRACKS;
    NTRACKS_REG2 <= NTRACKS_REG1;
  end if;

end process;


-- Initialize control signals (reset & start)

init : process(CLK)
  constant maxCnt : INTEGER := 127;
  variable count : INTEGER RANGE 0 to maxCnt := 0;
begin
  if rising_edge(CLK) then
    IF (USE_BUTTON) then
      START_HIT <= START_BTN;
    else
      if count < maxCnt then
        count := count + 1;
        -- Unset "reset" signal a few clock cycles after the FPGA is programmed.
        if count = 31 then
          RESET <= '0';
        elsif count = maxCnt then
          START_HIT <= '1';
        end if;  
      end if; 
    end if;
  end if;
end process;


-- Detect a press of central button on board.

button : entity work.button
  generic map (
    USE_BUTTON => USE_BUTTON
  )
  port map (
    CLK => CLK,
    BTN => BTN,
    PRESSED => START_BTN
  );


-- FIFO memory containing input hits (ready from data.txt).

readHits : entity work.readHitsIP
  port map (
     AP_CLK => CLK,
     AP_RST => RESET_HIT,
     AP_START => START_HIT,
     AP_DONE => READY_HIT,
     NEXTHIT => HIT,
     LAST(0) => LAST_HIT --! Indicates if this is the last hit available
);


-- BRAM memory for storing reconstructed tracks.

memTrks : entity work.memory
  generic map (
     WIDTH_BITS => WIDTH_BITS,
     DEPTH_BITS => DEPTH_BITS
  )
  port map (
     READ_CLK => CLKslow,
     READ_ENABLE => READ_ENABLE,
     READ_ADDR => READ_ADDR,
     READ_DATA => READ_DATA,
     WRITE_CLK => CLK,
     WRITE_ENABLE => WRITE_ENABLE,
     WRITE_ADDR => WRITE_ADDR,
     WRITE_DATA => WRITE_DATA
  );


-- Reconstruct tracks

trkReco : entity work.trkRecoIP
  port map (
     AP_CLK => CLK,
     AP_RST => RESET,
     AP_START => START_TRK,
     AP_DONE => READY_TRK,
     HIT => HIT,
     LAST_HIT(0) => LAST_HIT,
     DONE_HITS_READ(0) => DONE_TRK_READ_HITS, -- For debug only
     DONE_TRK_RECO(0) => DONE_TRK,
     NTRACKS => NTRACKS,
     TRACKS_ADDRESS0 => WRITE_ADDR,
     TRACKS_WE0 => WRITE_ENABLE,
     TRACKS_D0 => WRITE_DATA
  );


-- Display number of reconstructed tracks on 7-segment display.

displays : entity work.disp_nexys_a7_IP
  port map (
    AP_CLK => CLKslow,
    AP_RST => '0',
    AP_START => '1',
    AP_DONE => OPEN,
    AP_IDLE => OPEN,
    AP_READY => OPEN,
    VALUE_R => DISPLAY_IN_VALUE,
    PSEL => DISPLAY_SEL,
    PDISPLAY => DISPLAY_OUT
  );


-- Transmit data of reconstructed tracks over USB/UART link to Linux.

uart : entity work.sendDataIP
  port map (
     AP_CLK => CLKslow,
     AP_RST => RESET_REG2,
     AP_START => FINISHED_TRK_REG2,   -- Start this when tracking is finished
     AP_DONE => READY_UART,     
     DONE(0) => DONE_UART,
     nDATA => NTRACKS_REG2,
     DATA_ADDRESS0 => READ_ADDR,
     DATA_CE0 => READ_ENABLE,
     DATA_Q0 => READ_DATA,
     UART_TXD(0) => UART_TX
  );

end Behavioral;
