library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity memory is
    Generic ( WIDTH_BITS : natural;
    	      DEPTH_BITS : natural);
    Port ( CLK           : in  std_logic;
	   READ_ENABLE   : in  std_logic;
	   READ_ADDR     : in  std_logic_vector(DEPTH_BITS - 1 downto 0);
	   READ_DATA     : out std_logic_vector(WIDTH_BITS - 1 downto 0);
           READ_NENTRIES : out std_logic_vector(DEPTH_BITS - 1 downto 0);
	   WRITE_ENABLE  : in  std_logic;
	   WRITE_ADDR    : in  std_logic_vector(DEPTH_BITS - 1 downto 0);
	   WRITE_DATA    : in  std_logic_vector(WIDTH_BITS - 1 downto 0));
end memory;

architecture Behavioral of memory is

  constant DEPTH : natural := DEPTH_BITS**2;
  type memType is array(0 to DEPTH_BITS-1) of std_logic_vector(WIDTH_BITS-1 downto 0);
  signal RAM : memType := (others => (others => '0'));
  attribute ram_style : string;
  attribute ram_style of RAM : signal is "block";

  signal NENT : std_logic_vector(DEPTH_BITS - 1 downto 0) := (others => '0');
  
begin

memAccess : process (CLK)
begin

  if rising_edge(CLK) then
    -- Use register to help get across clock boundary.
    if READ_ENABLE = '1' then
       READ_DATA <= RAM(to_integer(unsigned(READ_ADDR)));
       READ_NENTRIES <= NENT;
    end if;

    if WRITE_ENABLE = '1' then
       assert (to_integer(unsigned(NENT)) < DEPTH) report "Error: BRAM memory too small" severity failure;
       RAM(to_integer(unsigned(WRITE_ADDR))) <= WRITE_DATA; 
       NENT <= std_logic_vector(unsigned(NENT) + 1);
    end if;
  end if;
end process;  

end Behavioral;
