----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.05.2021 13:27:18
-- Design Name: 
-- Module Name: tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Provides arithmetic for signed & unsigned values
--use IEEE.NUMERIC_STD.ALL;

entity tb is
--  Port ( );
end tb;

architecture Behavioral of tb is
constant CLK_PERIOD : time := 10 ns; --! 100 MHz clock from Nexys-A7-100T board
signal CLK100MHZ  : std_logic := '0'; --! Differential clock
signal BTN    : std_logic := '0'; --! Push button
signal UART_TX : std_logic := '0'; --! UART/USB transmit signal
signal LED    : std_logic_vector (4 downto 0) := (others => '0'); --! LED lights 
signal DISPLAY_SEGMENTS : std_logic_vector(7 downto 0) := (others => '0'); --! DISPLAY SEGMENTS.
signal DISPLAY_SEL : std_logic_vector(7 downto 0) := (others => '0'); --! DISPPLAY SEL CONTROL.

begin

CLK100MHZ <= not CLK100MHZ after CLK_PERIOD/2;

-- Button on board used to launch track reconstruction.
BTN <= not BTN after 1000ns;

mut : entity work.top
  port map (
     CLK100MHZ => CLK100MHZ,
     BTNC => BTN,     
     UART_RXD_OUT => UART_TX, -- Nexys-A7-100T confusingly names UART transmit "RX"
     LED_0  => LED(0),
     LED_1  => LED(1),
     LED_2  => LED(2),
     LED_3  => LED(3),
     LED_4  => LED(4),
     CA => DISPLAY_SEGMENTS(0),
     CB => DISPLAY_SEGMENTS(1),
     CC => DISPLAY_SEGMENTS(2),
     CD => DISPLAY_SEGMENTS(3),
     CE => DISPLAY_SEGMENTS(4),
     CF => DISPLAY_SEGMENTS(5),
     CG => DISPLAY_SEGMENTS(6),
     DP => DISPLAY_SEGMENTS(7),
     AN => DISPLAY_SEL     
  );

end Behavioral;
