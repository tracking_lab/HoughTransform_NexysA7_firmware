#---------------------------------------------------------------
# Compile HLS IP core for control display 7-segment of Nexys 7
#---------------------------------------------------------------

delete_project WorkHLS_display
open_project WorkHLS_display

set CFLAGS {-std=c++11}

set_top disp_nexys_a7
add_files     src_displays/disp_nexys_a7.h     -cflags "$CFLAGS"
add_files     src_displays/disp_nexys_a7.cc    -cflags "$CFLAGS"
add_files     src_displays/bin2bcd.h           -cflags "$CFLAGS"
add_files     src_displays/bin2bcd.cc          -cflags "$CFLAGS"
add_files     src_displays/to7segment.h        -cflags "$CFLAGS"
add_files     src_displays/to7segment.cc       -cflags "$CFLAGS"
add_files -tb src_displays/tb_display.cc       -cflags "$CFLAGS"

set solution solution1
delete_solution $solution
open_solution -flow_target vivado $solution

# NEXYS 7-T100 FPGA
set_part {xc7a100tcsg324-1}

# Frequency of CLKslow in top.vhd
create_clock -period 6.25MHz -name default

# Allow HLS to use longer (easier to understand) variable names in reports.
config_compile -name_max_length 100
# Register output signals, to ensure they're stable (at cost of +1 latency).
config_interface -register_io scalar_out 

csim_design -clean -mflags "-j8"
csynth_design
# Don't run cosim: it's too slow as DISPLAYS runs for many clock cycles.
# cosim_design -trace_level all
export_design -format ip_catalog -version 0.0.0

exit
