#ifndef __trkReco__
#define __trkReco__

#include "ap_int.h"
#include "hls_math.h" // Provides hls::exp(), hls::pow(), hls::abs()

// Number of bits in hit word
#include "../src_common/hitFormat.h"
// Parameters defining size of BRAM memory for storing tracks
#include "../src_common/trkMemorySpec.h"

// Magnetic field & speed of light.
constexpr float bField = 4.0;
constexpr float cLight = 3.0e8;

// Minimum number of hits in HT bin required for track candidate.
static const ap_uint<8> minHits = 9;

// Number of bins in Hough Transform (HT).
// (Should both be power of 2).
constexpr int nBinsR = 16, nBinsPhi0 = 128;
static const ap_int<16> minR = -nBinsR/2    , minPhi0 = -nBinsPhi0/2;
static const ap_int<16> maxR =  nBinsR/2 - 1, maxPhi0 =  nBinsPhi0/2 - 1;

// Bit shifts to correct for relative granularity of stub coords.
// & helix params.
// Adjusting (0,13) here will change the HT range in (1/R,phi0)
static const int shiftInvR =  0+hls::round(hls::log2(1.*nBinsR));
static const int shiftPhi0 = 13+hls::round(hls::log2(1./nBinsPhi0));

// Reconstruct tracks
//
// Input: hit (injected each clock cycle) & boolean indicating if no more
//        hits available.
// Output: doneHitsRead=1 when finished reading all hits;
//         done_trk_reco=1 when finished reconstructing tracks;
//         nTracks = number of tracks reconstructed;
//         tracks[] = reconstructed tracks.

void trkReco(ap_uint<NBITS_HIT> hit, ap_uint<1> last_hit, 
             ap_uint<1>& done_hits_read, ap_uint<1>& done_trk_reco, 
             ap_uint<NBITS_DEPTH>& nTracks, 
             ap_uint<NBITS_TRK> tracks[ramDepth_]);

#endif
