#include "trkReco.h"

// Hits
#include "../data/qOverPt_0.6/hits.h"
// Generated particles
#include "../data/qOverPt_0.6/particles.h"

#ifndef __SYNTHESIS__
#include <iostream>
#include <algorithm>
#include <cmath>
#endif

//--- Functions to convert binned (invR,phi0) from HT to floating point

float invR_flt(int invR) {return (float(invR)+0.5)*2/float(1<<shiftInvR);}
float phi0_flt(int phi0) {return (float(phi0)+0.5)*hit_granularity*float(1<<shiftPhi0);}
float qOverPt_flt(int invR) {return invR_flt(invR)/(bField*cLight/1.0e9);}

//--- Functions to determine accuracy of helix param reconstruction
//--- by comparison with truth particle.
float deltaPhi(unsigned int iPart, float phi0_flt) {
  constexpr float PI = 3.14159265;
  return fabs(fmod(true_phi0[iPart] - phi0_flt + PI, 2*PI) - PI);
} 
float deltaQoverPt(unsigned int iPart, float qOverPt_flt) {
  return fabs(true_qOverPt[iPart] - qOverPt_flt);
}

//--- Function to check if true particle matches reconstructed track
bool match(unsigned int iPart, float phi0_flt, float qOverPt_flt) {
  constexpr float phi0_cut = 0.1, qOverPt_cut = 0.5;
  return deltaPhi(iPart, phi0_flt) < phi0_cut and deltaQoverPt(iPart, qOverPt_flt) < qOverPt_cut;
}

//--- Main test bench.

int main(int argc, char **argv) {

  // Optionally consider at most this number of truth particles 
  // & their associated hits.
  const unsigned int maxParticles = 999;

  // RAM memory.
  ap_uint<NBITS_TRK> tracks[ramDepth_];

  ap_uint<1> last_hit = 0;
  ap_uint<1> doneHitsRead = 0, doneTrkReco = 0;
  ap_uint<NBITS_DEPTH> nTracks = 0;

  ap_uint<NBITS_HIT> inputHit = nullHit;

  unsigned int nErr = 0;  

  const unsigned int nLoops = 10000;

  // Boolean indicating if each truth particle was reconstructed
  bool particle_found[nParticles] = {false};
  float smallest_deltaPhi0[nParticles];
  float smallest_deltaQoverPt[nParticles];
  for (unsigned int iPart = 0; iPart < nParticles; iPart++) {
    smallest_deltaPhi0[iPart] = 999.;
    smallest_deltaQoverPt[iPart] = 999.;
  }

  for (unsigned int i = 0; i < nLoops; i++) {

    if (i == numHits - 1) {
      last_hit = 1;
    } else {
      // True particle that created stub. Shouldn't be used for track reco.
      ap_uint<NBITS_PART> nextParticle = hits[i+1].range(MSB_PART, LSB_PART); 
      last_hit = (nextParticle >= maxParticles);
    }

    // Optionally reduce number of particles giving hits in Tracker.
    ap_uint<NBITS_PART> iParticle = hits[i].range(MSB_PART, LSB_PART); 
    if (iParticle >= maxParticles) {
      inputHit = nullHit;
    } else {
      inputHit = hits[i];
    }

    // Reconstruct tracks;
    trkReco(inputHit, last_hit, doneHitsRead, doneTrkReco, nTracks, tracks);

    if (doneHitsRead) {
      // All hits read by tracking algorithm
      static bool firstHR = true;
      if (firstHR) std::cout<<std::endl<<"Hit reading by tracking algorithm finished at function call "<<std::dec<<i<<std::endl;
      firstHR = false;
    }

    if (doneTrkReco) {
      // All tracks reconstructed. Check if they are correct.

      std::cout<<std::endl<<"Tracking finished at function call "<<std::dec<<i<<"; found "<<nTracks<<" tracks"<<std::endl;

      // Sort tracks by phi0 for convenience.
      auto trkPhi0 = [](ap_uint<32> a, ap_uint<32> b) {return bool(ap_int<16>(a.range(15,0)) < ap_int<16>(b.range(15,0)));}; 
      std::sort(std::begin(tracks), std::begin(tracks)+nTracks, trkPhi0);

      for (unsigned int j = 0; j < nTracks; j++) {
        ap_int<16> invR = tracks[j].range(31,16); 
        ap_int<16> phi0 = tracks[j].range(15,0); 

        std::cout<<"Track="<<std::dec<<std::setw(3)<<j
                 <<" word="<<std::hex<<std::setw(10)<<tracks[j]<<std::dec
                 <<" digi(invR,phi0)=("<<std::setw(5)<<invR<<","<<std::setw(5)<<phi0<<")"
                 <<" flt(q/Pt,phi0)=("<<std::fixed<<std::setw(6)<<std::setprecision(3)
                 <<qOverPt_flt(invR)<<","<<phi0_flt(phi0)<<")"<<std::endl;       

        // Check which true particles this might correspond to
        // and how accurate the reconstructed helix params are.
        for (unsigned int iPart = 0; iPart < nParticles; iPart++) {
          if (match(iPart, phi0_flt(phi0), qOverPt_flt(invR))) {
            particle_found[iPart] = true;
            smallest_deltaPhi0   [iPart] = std::min(deltaPhi(iPart, phi0_flt(phi0)), smallest_deltaPhi0[iPart]);
            smallest_deltaQoverPt[iPart] = std::min(deltaQoverPt(iPart, qOverPt_flt(invR)), smallest_deltaQoverPt[iPart]);
          }
        }

      }

      std::cout<<std::endl<<"--- Info for HT parameter tuning ---"<<std::endl;
      std::cout<<"Bit shifts: shiftInvR="<<shiftInvR<<" shiftPhi0="<<shiftPhi0<<std::endl;
      std::cout<<"HT range in q/Pt: "<<qOverPt_flt(minR)<<" to "<<qOverPt_flt(maxR)<<" with bin size "<<qOverPt_flt(nBinsR)/nBinsR<<std::endl;
      std::cout<<"HT range in phi0: "<<phi0_flt(minPhi0)<<" to "<<phi0_flt(maxPhi0)<<" with bin size "<<phi0_flt(nBinsR)/nBinsPhi0<<std::endl<<std::endl;

      break;
    }
  }

  // Print efficiency to reconstruct particles (in particles.h).
  unsigned int nFound = 0;
  float mean_deltaQoverPt = 0.;
  float mean_deltaPhi0 = 0.;
  for (unsigned int i = 0; i < std::min(nParticles, maxParticles); i++) {
    if (particle_found[i]) {
      nFound++;
      mean_deltaQoverPt += smallest_deltaQoverPt[i];
      mean_deltaPhi0    += smallest_deltaPhi0[i];
    } else {
      std::cout<<"Failed to reconstruct truth particle "<<i<<" with (q/Pt, phi0)=("<<true_qOverPt[i]<<","<<true_phi0[i]<<")"<<std::endl;
    }
  }
  std::cout<<std::endl<<"Efficiency to reconstruct particles = "
           <<nFound<<"/"<<std::min(nParticles, maxParticles)<<std::endl;

  if (nFound > 0) {
    // Print helix parameter resolution.
    mean_deltaQoverPt /= float(nFound);
    mean_deltaPhi0    /= float(nFound);
    std::cout<<"Resolution in flt(q/Pt, phi0) = ("
             <<mean_deltaQoverPt<<","<<mean_deltaPhi0<<")"<<std::endl<<std::endl;
  }

  nErr = 0;
    
  return nErr;
}

  

