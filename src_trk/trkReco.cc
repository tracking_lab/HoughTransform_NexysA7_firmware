#include "trkReco.h"

#ifndef __SYNTHESIS__
#include <iostream>
#include <sstream>
#endif

//--- Reconstruct tracks
void trkReco(ap_uint<NBITS_HIT> hit, ap_uint<1> last_hit, 
             ap_uint<1>& done_hits_read, ap_uint<1>& done_trk_reco, 
             ap_uint<NBITS_DEPTH>& nTracks, 
             ap_uint<NBITS_TRK> tracks[ramDepth_]) {
#pragma HLS INTERFACE ap_none port=done_hits_read
#pragma HLS INTERFACE ap_none port=done_trk_reco
#pragma HLS INTERFACE ap_none port=nTracks
// Code below is only capable of accepting a new hit every 2 clock cycles.
#pragma HLS PIPELINE II=2

  //-------------------------------------------------------------------
  // THEORY
  // Eqn. of track circle: phi = phi0 - 0.5*r/R,
  // where track radius of curvature R = alpha*q*Pt 
  // where  alpha = B*c/1e9, r in metres, Pt in GeV & q=+-1.
  //
  // Look for tracks with Hough Transform counting stubs in 2D array
  // of (1/R, phi0)
  //-------------------------------------------------------------------

  // Array to count stubs in each HT bin.
  static ap_uint<8> nHitsInBin[nBinsR][nBinsPhi0] = {0};

  // Array storing phi0 bin of all reco tracks in given 1/R bin.
  static const unsigned int MAXTRK=16; // Assumed max. per 1/R bin
  static ap_int<16> phi0TrksInBinR[nBinsR][MAXTRK] = {0};
  // Array storing number of reco tracks in each given 1/R bin.
  static ap_uint<8> nTrksInBinR   [nBinsR]     = {0};

  // (The pragma statement say each column in q/Pt should be partitioned into
  // a separate memory in the FPGA -- Important as FPGA can only read a 
  // memory once per clock cycle).
#pragma HLS ARRAY_PARTITION variable=nHitsInBin complete dim=1
#pragma HLS ARRAY_PARTITION variable=phi0TrksInBinR complete dim=1
#pragma HLS ARRAY_PARTITION variable=nTrksInBinR complete dim=1

  static ap_uint<NBITS_DEPTH+1> index = 0;
  static ap_uint<1> done_hits_read_tmp = 0;
  static ap_uint<1> done_trk_reco_tmp = 0;
  static ap_uint<1> printHT = 1;

  if (not done_hits_read_tmp) {

    //-- Process additional hits

    // Unpack next hit.
    ap_int <NBITS_PHI>  phi    = hit.range(MSB_PHI , LSB_PHI);
    ap_int <NBITS_Z>    z      = hit.range(MSB_Z   , LSB_Z);
    ap_int <NBITS_R>    r      = hit.range(MSB_R   , LSB_R);
    ap_uint<NBITS_LAY>  layer  = hit.range(MSB_LAY , LSB_LAY);
    ap_uint<NBITS_EVT>  iEvent = hit.range(MSB_EVT , LSB_EVT);
    // True particle that created stub. Shouldn't be used for track reco.
    ap_uint<NBITS_PART> iParticle = hit.range(MSB_PART, LSB_PART); 

    for (ap_int<16> invR = minR; invR <= maxR; invR++) {
      // Calculate phi0 bin consistent with stub and this invR bin.
      // (Ignores fact that there > 1 phi0 bin may be consistent).
      ap_int<32> phi0 = (phi + ((r*invR)>>shiftInvR))>>shiftPhi0;

      if (phi0 >= ap_int<32>(minPhi0) and phi0 <= ap_int<32>(maxPhi0)) {

        // Add this hit to Hough Transform array.
        nHitsInBin[invR-minR][ap_int<16>(phi0)-minPhi0]++;

        // Check if this bin now forms a track candidate, 
        // and store it if it does.
        // N.B. Can't read & write from HT array BRAM in single clock
        // cycle, which explains II=2 above.
        if (nHitsInBin[invR-minR][ap_int<16>(phi0)-minPhi0] == minHits) {
          ap_uint<16> num = nTrksInBinR[invR-minR]++;
#ifndef __SYNTHESIS__
          assert(num < MAXTRK); // If fails, increase MAXTRK
#endif
          phi0TrksInBinR[invR-minR][num] = ap_int<16>(phi0);
        }
      }

#ifndef __SYNTHESIS__
          std::cout<<std::dec<<"Fill: HT (invR,phi0) bin=("<<std::setw(5)<<invR<<","<<std::setw(6)<<phi0<<")"
                   <<" with phi0_corr= "<<std::setw(6)<<((r*invR)>>(shiftInvR+shiftPhi0))
                   <<" layer="<<std::setw(2)<<layer
                   <<" particle="<<std::setw(4)<<iParticle<<std::endl;
#endif
    }

    done_hits_read_tmp = last_hit;

  } else if (not done_trk_reco_tmp) {

    //-- No more hits. Search HT bins are track candidates.

    static ap_uint<16> idx = 0;
    static ap_int<16> bin_invR = minR;
    ap_uint<16> num = nTrksInBinR[bin_invR - minR];
    tracks[index] = (bin_invR, phi0TrksInBinR[bin_invR - minR][idx]);
    if (idx < num) {
      index++;
      idx++;
    } else if (bin_invR <maxR) {
      bin_invR++;;
      idx = 0;         
    } else {
      done_trk_reco_tmp = 1;
    }

#ifndef __SYNTHESIS__
    if (printHT) {
      printHT = 0;
      std::cout<<"Hough Transform array: no. of entries"<<std::endl;
      for (ap_int<16> phi0 = minPhi0; phi0 <= maxPhi0; phi0++) {
        bool emptyRow = true;
        std::ostringstream strRow;
        strRow<<std::dec<<"phi0 column="<<std::setw(4)<<phi0<<": ";
        for (ap_int<16> invR = minR; invR <= maxR; invR++) {
          if (nHitsInBin[invR-minR][phi0-minPhi0] > 0) emptyRow = false;        
          strRow<<std::dec<<std::setw(3)<<nHitsInBin[invR-minR][phi0-minPhi0];
        }
        if (not emptyRow) std::cout<<strRow.str()<<std::endl;
      }
    }
#endif
  }
   
  done_hits_read = done_hits_read_tmp; // Finished reading hits
  done_trk_reco  = done_trk_reco_tmp; // Finished reconstructing tracks.
  nTracks = index;
}
  
