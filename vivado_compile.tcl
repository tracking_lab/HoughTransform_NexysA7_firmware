#------------------------------------------------------------------------------
# Open the Vivado project produced by vivado_sim.tcl .
# Synthesize & implement the algorithm, and produce 
# top.bit for loading into the FPGA.
#------------------------------------------------------------------------------

set projName WorkVHDL

set_param general.maxThreads 8

open_project ${projName}/${projName}.xpr

# Compile: Synthesis (with reduced CPU strategy)
set_property "strategy" "Flow_RuntimeOptimized" [get_runs synth_1]
launch_runs synth_1 -jobs 4
wait_on_run synth_1

# Check if synthesis ran successfully (contains "synth_design Complete!")
set RA [get_property STATUS [get_runs synth_1] ]
puts "=== SYNTHESIS STATUS = $RA"

# Compile: Implementation & .bit file generation (with reduced CPU strategy).
set_property "strategy" "Flow_RuntimeOptimized" [get_runs impl_1] 
launch_runs impl_1 -jobs 4 -to_step write_bitstream
wait_on_run impl_1
open_run impl_1
report_timing_summary -file  ${projName}/timing_impl.rpt
report_utilization -hierarchical -file  ${projName}/utilization_impl.rpt

# Check if implementation ran successfully (contains "impl_design Complete!")
set RA [get_property STATUS [get_runs impl_1] ]
# Check if it met timing.
set WHS [get_property SLACK [get_timing_paths -hold]]
set WSS [get_property SLACK [get_timing_paths -setup]]
puts "=== IMPLEMENTATION STATUS = $RA , TIMING: WHS = $WHS, WSS = $WSS"
if {$WHS < 0 || $WSS < 0} {
  puts "ERROR: implentation failed timing."
  exit 1
}
