#---------------------------------------------------------------
# Compile HLS IP core for reconstructing tracks from hits
# and writing them to BRAM memory.
#---------------------------------------------------------------

delete_project WorkHLS_trk
open_project WorkHLS_trk

set CFLAGS {-std=c++11}

set_top trkReco
add_files     src_common/trkMemorySpec.h  -cflags "$CFLAGS"
add_files     src_trk/trkReco.h           -cflags "$CFLAGS"
add_files     src_trk/trkReco.cc          -cflags "$CFLAGS"
add_files -tb src_trk/tb.cc               -cflags "$CFLAGS"

set solution solution1
delete_solution $solution
open_solution -flow_target vivado $solution 

# Nexys-A7 100T
set_part {xc7a100tcsg324-1}

create_clock -period 100MHz -name default

# Allow HLS to use longer (easier to understand) variable names in reports.
config_compile -name_max_length 100
# Register output signals, to ensure they're stable (at cost of +1 latency).
config_interface -register_io scalar_out 

csim_design -clean -mflags "-j8"
csynth_design
cosim_design -trace_level all
export_design -format ip_catalog -version 0.0.0

exit
