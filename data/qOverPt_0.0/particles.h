const unsigned int nParticles=3;
float true_phi0   [nParticles] = {  -2.493,  -1.658,  -0.653,};
float true_tanL   [nParticles] = {  -0.690,   0.099,  -0.556,};
float true_qOverPt[nParticles] = {   0.011,  -0.013,   0.015,};
float true_invR   [nParticles] = {   0.013,  -0.015,   0.018,};
