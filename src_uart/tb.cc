#include "sendData.h"

#ifndef __SYNTHESIS__
#include <iostream>
#endif

int main(int argc, char **argv) {

  static const ap_uint<NBITS_TRK> dataStore[] = {0x8D1625AC, 0xA7412BF19, 0x57, 0x2A755, 0x2D1EC916};
  static const ap_uint<NBITS_DEPTH> nData = sizeof(dataStore)/sizeof(dataStore[0]);
  ap_uint<NBITS_TRK> data[ramDepth_];
  assert(nData < ramDepth_);
  for (ap_uint<NBITS_DEPTH> i = 0; i < nData; i++) data[i] = dataStore[i];

  ap_uint<1> done = 0;
  ap_uint<1> uart_txd = 0;
  ap_uint<1> uart_txd_last = 1;

  // To transmit five 32b numbers, with factor 5 safety factor. 
  const unsigned int nLoops = 5*5*32*(freq_FPGA_/freq_baud_rate_);
  unsigned int nChange = 0;

  std::cout<<"NLOOPS="<<nLoops<<std::endl;
  
  for (unsigned i = 0; i < nLoops; i++) {
    
    sendData(nData, data, done, uart_txd);

    if (uart_txd_last != uart_txd) std::cout<<"TB: i="<<std::dec<<i<<" done="<<done<<" uart_txd="<<uart_txd<<std::endl;

    if (uart_txd_last != uart_txd) {
      nChange++;
      uart_txd_last = uart_txd;
    }

    if (done == '1') break;
  }

  unsigned int nErr = 0;
  const unsigned int nChangeExpected = 292; // Set according to C++ Sim results.
  if (nChange != nChangeExpected) nErr = 1;
  std::cout<<"uart_txd changed state "<<std::dec<<nChange<<" times vs "<<nChangeExpected<<" expected"<<std::endl;

  return nErr;
}

  

