#include "sendData.h"

#ifndef __SYNTHESIS__
#include <iostream>
#endif

//--- Transmit nData 32b words from data[] over UART/USB

void sendData(ap_uint<NBITS_DEPTH> nData, const ap_uint<NBITS_TRK> data[ramDepth_],
	      ap_uint<1>& done, ap_uint<1>& uart_txd) {
#pragma HLS INTERFACE ap_none port=done
#pragma HLS INTERFACE ap_none port=uart_txd

// IMPORTANT: To generate correct baud rate data output, we assume this function is called once per
// FPGA clock cycle. Please check that it synthesises with II=1, or it will not work.
#pragma HLS PIPELINE II=1

  // Ascii codes for the 16 hex numbers 0-F.
  static const ap_uint<8> hexToAscii[] = {0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x39, 0x39,
			                  0x41, 0x42, 0x43, 0x44, 0x45, 0x46};
  // Partitioning array improves pipelining.
#pragma HLS ARRAY_PARTITION variable=hexToAscii complete dim=1

  // The two ascii codes for new line
  enum {NUMCR=2};
  static const ap_uint<8> LF = 0x0A;
  static const ap_uint<8> CR = 0x0D;

  static ap_uint<NBITS_TRK> dataWord_reg = 0;
  static ap_uint<8> ascii_reg = 0;

  // Index to data word
  static ap_uint<NBITS_DEPTH+1> index = 0;
  
  // Index controlling stepping through the hex elements of each data word.
  static ap_int<8> iLoop = NHEX_TRK - 1;

  ap_uint<8> ascii = 0;
  ap_uint<1> ascii_vld = 0;
  ap_uint<1> ascii_accepted = 0;

  // Finite state machine (improves pipelining).
  enum class DataAccess {GET_WORD, GET_ASCII, VALIDATE};
  static DataAccess dataState = DataAccess::GET_WORD;

  done = (nData > 0 && index == nData);
  
  if (iLoop >= 0) {
    // Convert each hex digit "iLoop" of current data word to corresponding Ascii code.
    // (Do this over 3 clock cycles (=function calls) to improve pipelining.
    if (dataState == DataAccess::GET_WORD) {
      if (index < nData) {
        dataWord_reg = data[index];
#ifndef __SYNTHESIS__
        std::cout<<"READ FROM ARRAY "<<std::hex<<dataWord_reg<<std::endl;
#endif
        dataState = DataAccess::GET_ASCII;
      }
      ascii_vld = 0;
    } else if (dataState == DataAccess::GET_ASCII) {
      ap_uint<8> iHex = ap_uint<8>(iLoop);
      ap_uint<8> offset = 4*iHex;
      ap_uint<4> d = dataWord_reg.range(offset + 3, offset);
      ascii_reg = hexToAscii[d];
      ascii_vld = 0;
      dataState = DataAccess::VALIDATE;
    } else if (dataState == DataAccess::VALIDATE) {
      ascii = ascii_reg;
      ascii_vld = 1;
    }
      
  } else {
    // iHex = -1 or -2 indicates ready to add Ascii new line characters.
    ascii = (iLoop == -1) ? LF : CR;
    ascii_vld = 1;
  }


  // Transmit Ascii symbol.
  sendAscii(ascii, ascii_vld, ascii_accepted, uart_txd);  

  // Prepare for next loop.
  if (ascii_vld && ascii_accepted) {
#ifndef __SYNTHESIS__
    std::cout<<"SENT ASCII: index=("<<std::dec<<index<<","<<iLoop<<") ascii="<<std::hex<<ascii;
    if (iLoop >= 0) std::cout<<" data="<<std::hex<<dataWord_reg<<std::endl;
    if (iLoop < 0)  std::cout<<" new line "<<std::endl;;
#endif
    if (iLoop > -NUMCR) {
      // Get next hex element forming current data word.
      dataState = DataAccess::GET_ASCII;
      iLoop--;
    } else {
      // Get next data word in array.
      if (index < nData) index++;
      dataState = DataAccess::GET_WORD;
      iLoop = NHEX_TRK - 1;
    }
  }
}

//--- Transmit single Ascii symbol (4 bits) over UART/USB

void sendAscii(ap_uint<8> ascii, ap_uint<1> ascii_vld,
	       ap_uint<1>& ascii_accepted, ap_uint<1>& uart_txd) {

  enum {nBitsPacket=8+2};

  // Finite state machine
  enum class State {WAIT_ASCII, SEND_PACKET};
  
  static State state = State::WAIT_ASCII;
  
  static ap_uint<8> iBit = 0;
  static ap_uint<nBitsPacket> packet_reg = 0;
  static ap_uint<1> uart_txd_reg = 1;

  ap_uint<1> clock_tick_baud = clk_tick_baud();
  
  ascii_accepted = 0;
  
  switch (state) {

    case State::WAIT_ASCII:
      if (ascii_vld) {
        state = State::SEND_PACKET;
	ascii_accepted = 1;
        packet_reg = (ap_uint<1>(1), ascii, ap_uint<1>(0));
      }
      break;      
  
    case State::SEND_PACKET:
      if (clock_tick_baud) {
        if (iBit < nBitsPacket) {  
  	  uart_txd_reg = packet_reg[iBit];
#ifndef __SYNTHESIS__
	  std::cout<<"SEND BIT: addr="<<std::dec<<iBit<<" data="<<uart_txd_reg<<std::endl;
#endif
          iBit++;
        } else {
	  uart_txd_reg = UART_NULL;
          iBit = 0;
	  packet_reg = 0;
          state = State::WAIT_ASCII;
        }
      }
      break;
  }

  uart_txd = uart_txd_reg;
}

//--- Gives one tick each period of the UART/USB baud rate.

ap_uint<1> clk_tick_baud() {
  static const ap_uint<32> baudPeriodClks = (int) (freq_FPGA_/freq_baud_rate_); 
  static ap_uint<32> count = 0;
  static ap_uint<1> tick = 0;
  
  if (count < baudPeriodClks) {
    count++;
    tick = 0;
  } else {
    count = 0;
    tick = 1;    
  }  
  return tick;
}
  
  

