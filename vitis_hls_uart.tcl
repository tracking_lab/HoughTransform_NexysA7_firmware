#---------------------------------------------------------------
# Compile HLS IP core for transmitting data from FPGA to Linux
# over USB/UART link.
#---------------------------------------------------------------

delete_project WorkHLS_uart
open_project WorkHLS_uart

set CFLAGS {-std=c++11}

set_top sendData
add_files     src_common/trkMemorySpec.h -cflags "$CFLAGS"
add_files     src_uart/sendData.h        -cflags "$CFLAGS"
add_files     src_uart/sendData.cc       -cflags "$CFLAGS"
add_files -tb src_uart/tb.cc             -cflags "$CFLAGS"

set solution solution1
delete_solution $solution
open_solution -flow_target vivado $solution

# Nexys-A7 100T
set_part {xc7a100tcsg324-1}

# Frequency of CLKslow in top.vhd
create_clock -period 6.25MHz -name default

# Allow HLS to use longer (easier to understand) variable names in reports.
config_compile -name_max_length 100

csim_design -clean -mflags "-j8"
csynth_design
# Don't run cosim: it's too slow as UART runs for many clock cycles.
# cosim_design -trace_level all
export_design -format ip_catalog -version 0.0.0

exit
