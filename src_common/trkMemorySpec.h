#ifndef __trkMemorySpec__
#define __trkMemorySpec__

//--------------------------------------------------------
// Specify BRAM memory in which reconstructed tracks
// will be stored
//--------------------------------------------------------

// Size of each track data word in BRAM, in 4-bit hex words and in bits.
enum {NHEX_TRK=8, NBITS_TRK=4*NHEX_TRK};

// Depth of BRAM (making this bigger slows down COSIM significantly)
enum {NBITS_DEPTH=8};
static const unsigned int ramDepth_ = (1 << NBITS_DEPTH);

#endif
