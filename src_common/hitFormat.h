#ifndef __hitFormat__
#define __hitFormat__

#include "ap_int.h"

// No. of bits of each variable packed into each digitized hit (in data/*/hits.h)
// (Most-significant bit first).
enum HitFormat {NBITS_PHI=16, NBITS_R=16, NBITS_Z=16, NBITS_LAY=4,  NBITS_EVT=4, NBITS_PART=8};

// Digitization granularity in r, phi & z.
constexpr float hit_granularity = 0.001;

// Least & most significant bit of each variable.
enum HitFormatLSB {
  LSB_PART   = 0,
  LSB_EVT    = LSB_PART + NBITS_PART,
  LSB_LAY    = LSB_EVT  + NBITS_EVT,
  LSB_Z      = LSB_LAY  + NBITS_LAY,
  LSB_R      = LSB_Z    + NBITS_Z,
  LSB_PHI    = LSB_R    + NBITS_R,
  MSB_PART   = LSB_EVT - 1,
  MSB_EVT    = LSB_LAY - 1,
  MSB_LAY    = LSB_Z   - 1,
  MSB_Z      = LSB_R   - 1,
  MSB_R      = LSB_PHI - 1,
  MSB_PHI    = LSB_PHI + NBITS_PHI - 1
};

enum {NBITS_HIT=MSB_PHI + 1};

static const ap_uint<NBITS_HIT> nullHit = 0;

#endif
